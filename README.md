# Tech6 Challenge

-   [Introduction](#introduction)
-   [Requirements](#requirements)
-   [Usage](#usage)
    -   [How to install dependencies](#how-to-install-dependencies)
    -   [How to run](#how-to-run)
-   [To Acknowledge](#to-acknowledge)
    -   [GitHub Access Key](#github-access-key)
-   [Creator](#creator)


## Introduction
This is a challenge created by Tech6 sent to San Mônico.

## Requirements
Based on the development and test environment. These are the mininum requirements.
I can not guarantee it is going to work on lower versions, but on higher versions it should work nicely.
-   Node / NodeJS version 10.12.0 or higher.
-   NPM version 6.4.1 or higher.

## Usage
### How to install dependencies
```bash
npm i
```
### How to run
```bash
npm start
```

## To Acknowledge
-   In the document, `organizations` wasn't described if wanted the names, URLs or other kind of information.
So I decided to bring up only the total count
-   In the document, `repositories` did not specify how many repositories nor how they should be ordered, like happend in other specifications.
So I brought up the 6 first pinned repositories.

### GitHub Access Key
This project is using `ReDemoNBR`'s personal **TEMPORARY** test key to access `GitHub` API services. **THIS KEY WILL BE REVOKED** ON `Sat 2018-11-03`, which means it will stop working on this date. After this date, you will have to generate and use your own key.

You can generate a new one [here](https://github.com/settings/tokens). It requires the following permissions:
-   `read:user`
-   `user:email`

After generating it, open the file `src/index.tsx` and update the value `AUTH_TOKEN`
in the first line of code.

Have fun coding :)


## Creator
Made by San Mônico (real name Alanderson dos Reis Mônico).  
san@monico.com.br