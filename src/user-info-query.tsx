import gql from "graphql-tag";

const USER_DATA_QUERY = gql`
query UserInfo($username: String!){
  user(login: $username) {
    name
    login
    avatarUrl
    followers(first: 5) {
      nodes {
        name
        email
      }
    }
    following {
      totalCount
    }
    gists {
      totalCount
    }
    organizations {
      totalCount
    }
    pinnedRepositories (orderBy: {field: NAME, direction: ASC}, first: 6) {
      totalCount
      nodes {
        name
        url
      }
    }
  }
}`;

export default USER_DATA_QUERY;