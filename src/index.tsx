const AUTH_TOKEN: string = "b4a2ebbb54062e4ffc0be2c06eae4525f66b640e";

import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import * as React from 'react';
import { ApolloProvider } from "react-apollo";
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';


const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: createHttpLink({
        headers: { authorization: `token ${AUTH_TOKEN}` },
        uri: "https://api.github.com/graphql",
    })
});


ReactDOM.render(
    <ApolloProvider client={client}><App /></ApolloProvider>,
    document.getElementById('root') as HTMLElement
);

registerServiceWorker();
