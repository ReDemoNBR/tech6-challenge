import * as React from 'react';
import {Query} from "react-apollo";
import './App.css';
import logo from './logo.svg';
import query from "./user-info-query";


const App: any = ({username = ""}) => {
    const updateUsername = (e: any) => username = e.target.value;
    return (
        <div>
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
            </div>
            <Query query={query} variables={{username}}>
                {({data, error, loading, refetch}) => {
                    if (loading) {
                        return "Loading";
                    }
                    if (!username || !data || !data.user){
                        return (
                            <div>
                                <div>Type the name of the user to lookup</div>
                                <input type="text"
                                    placeholder="username"
                                    onChange={updateUsername}
                                    onKeyUp={event=>/enter/i.test(event.key) && refetch({username})}
                                />
                                <button onClick={()=>refetch({username})}>Lookup</button>
                            </div>
                        );
                    }
                    if (error) {
                        return (
                            <div>
                                <div><strong>Error: ${error.message}</strong></div>
                                <div>
                                    <div>Type the name of the user to lookup</div>
                                    <input type="text"
                                        placeholder="username"
                                        onChange={updateUsername}
                                        onKeyUp={event=>/enter/i.test(event.key) && refetch({username})}
                                    />
                                    <button onClick={()=>refetch({username})}>Lookup</button>
                                </div>
                            </div>);
                    }
                    const {user} = data;
                    return (
                        <div>
                            <div>
                                <div>Type the name of the user to lookup</div>
                                <input type="text"
                                    placeholder="username"
                                    onChange={updateUsername}
                                    onKeyUp={event=>/enter/i.test(event.key) && refetch({username})}
                                />
                                <button onClick={()=>refetch({username})}>Lookup</button>
                            </div>
                            <p><strong>Name: </strong>{user.name} ({user.login})</p>
                            <img src={user.avatarUrl} width="50" height="50"/>
                            <ul>{user.followers.nodes.map((node: any)=>(<li>{node.name || "[no name]"} - ({node.email || "[no email]"})</li>))}</ul>
                            <p><strong>Following: </strong>{user.following.totalCount}</p>
                            <p><strong>Number of Gists: </strong>{user.gists.totalCount}</p>
                            <p><strong>Organizations: </strong>{user.organizations.totalCount}</p>
                            <p><strong>Pinned Repositories:</strong> {user.pinnedRepositories.totalCount}
                                <ul>{user.pinnedRepositories.nodes.map((node: any)=>(<li><a href={node.url}>{node.name}</a></li>))}</ul>
                            </p>
                        </div>
                    );
                }}
            </Query>
        </div>
    );
}

export default App;
